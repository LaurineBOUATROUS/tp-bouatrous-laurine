/* 
 * Fichier modèle pour l'édition d'un script
 */

//---------------------------------------------------------------------------
// Sources de données, constantes


//---------------------------------------------------------------------------
// Variables globales
var identifiant;
var motdepasse;
 
 

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// Programme principal avec jQuery
$(document).ready(main) ;           // attente chargement du DOM

function main() {                   // programme principal
    $("#btnConnexion").click(verifConnexion);  //Lors dun click sur le bouton connexion
}


//---------------------------------------------------------------------------
// Fonctions évènementielles
function verifConnexion(e){
    e.preventDefault();
    identifiant = $("#identifiant").val(); // récupère les valeurs de identifiant
    motdepasse = $("#motdepasse").val(); // récupère les valeurs de mot de mot de passe
    
    if((identifiant == "")&&(motdepasse == "")){    //si aucune valeur entree
        $("#codErr").html("Veuillez saisir vos identifiants");
    }
    
    if((identifiant != "")&&(motdepasse == "")){  //si aucun mot de passe entre
        $("#codErr").html("Veuillez saisir votre mot de passe");
    }
    
    if((identifiant == "")&&(motdepasse != "")){ //si aucun identifiant entre
        $("#codErr").html("Veuillez saisir votre identifiant");
    }
    
    if((identifiant != "")&&(motdepasse != "")){ //toutes donnee ayant etait fournis 
        document.location.href="127.0.0.1:82/application/controllers/C_Test.php";
    }

    
    
}




//---------------------------------------------------------------------------
// Fonctions utilitaires

