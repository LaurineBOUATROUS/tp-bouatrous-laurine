Pour codeigniter : 

Pour chaque chaque configuration il faut :

1. Changer le base_url dans le fichier config.php 

On met généralement l'alias ou le virtualhost voir le nom de dommaine complet.

2. Vérifier les url qu'utilise tes scripts javascript très important sinon il se pourrait qu'il ne fonctionnne plus (par exemple en ce qui concerne les requêtes AJAX) 

3. Si tu as un .htaccess (on en reparlera plus tard il faut aussi vérifier que l'url que utilise est bonne c'est souvent la même que dans le base_url en 1.). Sinon les règles de réecriture que tu écris dedans ne fonctionnera pas et généra une erreur venant du Serveur.

4. C'est tout pour le moment 