<section id="one" class="main special"> 
    
    <div class="container">
        
        <div class="content">
            
            <p><img src="<?= base_url(); ?>assets/images/logotourisme.jpg"/></p>
            
            <div class="espace2"></div>
            
            <header class="major">
                <h2>Bienvenue sur le site web de configuration</h2>
                <div class="espace2"></div>
            </header>   
            
            <p>Site web de configuration appartenant à l'office de tourisme afin d'ajouter des messages personnalisés.</p>
            
            <div class="espace2"></div>
            
            <img src="<?= base_url(); ?>assets/images/Capture.PNG" />
        
        </div>

        <header class="major">
            <h2>Connexion au site web configuration</h2>
        </header>
        
        <form method="POST" action="#"><!-- Formulaire de connexion (Methode POST)--> 
            
            <!--On récupère les valeurs du formulaire ci-dessous avec les variables Php comme ceci $_POST['login'] on prend le nom contenue dans la propriétés name de la balise input-->
            
            <div class="row gtr-uniform">
                
                <!-- $_POST['login'] car name="login" -->
                
                <div class="col-6 col-12-xsmall"><input type="text" name="login" id="identifiant" placeholder="Identifiant" /></div>
                
                <!-- $_POST['password'] car name="password" -->
                
                <div class="col-6 col-12-xsmall"><input type="password" name="password" id="motdepasse" placeholder="Mot de passe" /></div>
                
                <!--On inséra un message d'erreur dans la balise codErr -->
                
                <div id="codErr"></div>

                <div class="col-12">
                    <ul class="actions special">
                        <li><input type="submit" id="btnConnexion" value="Connexion"  class="primary" /></li>
                    </ul>
                </div>
            </div>
        </form>
        
    </div>
    
</section>

<section id="two" class="main special">
    
    <div class="container">

        <div class="content">
            
            <header class="major">
                
                <div class="espace2"></div>
                <img src="<?= base_url(); ?>assets/images/Capture.PNG" />  
                <div class="espace2"></div>
                <h2>INFORMATIONS DU SITE</h2>

            </header>
            
            <p>Ce site est dédié à la création de messages personnalisés. Vous pourrez les modifier, les supprimer selon vos besoins. Vous aurez aussi la possibilité de plannifier à l'avance vos messages en choisissant la date et l'heure de votre choix. </p>
            
            <ul class="icons-grid">
                
                <li>
                    <span class=" icon solid major far fa-window-close"></span><h3>Supprimer</h3>
                </li>
                
                <li>
                    <span class="icon solid major fa-pencil-alt"></span><h3>Modifier</h3>
                </li>

            </ul>
        
        </div>

    </div>

</section>

<section id="three" class="main special">
    <div class="container">

    </div>
</section>

