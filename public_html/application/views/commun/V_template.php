<!DOCTYPE HTML>
<html>
    <head>

        <title><?= $title ; ?></title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <link rel="stylesheet" href="<?= base_url(); ?>assets/css/main.css" />
    </head>
    <body class="is-preload">
        
        <section id="header">
            <header class="major">
                <h1>AUTHENTIFICATION</h1>
                <p>SITE WEB CONFIGURATION</p> 
                <p>Office de tourisme </p>
            </header>
        </section> 

        <?= $content_page ; ?>
        
        <section id="footer">
            <footer>
                <ul class="icons">
                    <li>&copy; Lycée Gustave Eiffel</li><li>Armentières </li><li>Année 2019/2020 </li>
                </ul>
            </footer>
        </section>
        
        <script src="<?= base_url(); ?>assets/assets/js/jquery.min.js"></script>
        <script src="<?= base_url(); ?>assets/js/jquery.scrollex.min.js"></script>
        <script src="<?= base_url(); ?>assets/js/jquery.scrolly.min.js"></script>
        <script src="<?= base_url(); ?>assets/js/browser.min.js"></script>
        <script src="<?= base_url(); ?>assets/js/breakpoints.min.js"></script>
        <script src="<?= base_url(); ?>assets/js/util.js"></script>
        <script src="<?= base_url(); ?>assets/js/main.js"></script>
        <script src="<?= base_url(); ?>assets/js/config.js"></script>
    </body>
</html>