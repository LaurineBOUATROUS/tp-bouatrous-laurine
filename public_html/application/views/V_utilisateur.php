<section id="one" class="main special"> 
    
    <div class="container">
        
        <div class="content">

        <?php if(isset($_SESSION['user']) and !empty($_SESSION['user'])) { ?>

        <h3><a href="<?= site_url(); ?>/C_config/disconnect">Déconnexion</a></h3>

        <?php } ?>  
            
            <p><img src="<?= base_url(); ?>assets/images/logotourisme.jpg"/></p>
            
            <div class="espace2"></div>
            
            <header class="major">
            
                <h2>Bienvenue sur le site web de configuration</h2>
                
                <div class="espace2"></div>

            </header> 

            <div class="list-group">

            <?php foreach($resultMsg as $msg) { ?>  <!--- Foreach pour afficher la liste des messages recu de la base données -->

                <a href="#" class="list-group-item list-group-item-action flex-column align-items-start active">

                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1"><?= $msg['message']; ?></h5>
                        <small>Date de début : <?= $msg['dateDebut']; ?></small><br>
                        <small>Date de fin : <?= $msg['dateFin']; ?></small><br>
                    </div>

                   <br>

                </a>

            <?php } ?> 
            
            <p>Utilisateur <?= $_SESSION['user']['login'] ; ?> connecté</p>
        
        </div>
        
    </div>
    
</section>