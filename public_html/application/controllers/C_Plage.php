<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Plage extends CI_Controller {

	public function __construct() 
    {
        
        parent::__construct();
        
        $this->load->model('M_connexion') ;
        
        $this->load->helper('url');
            
    }
    
	public function index()
	{  
        
        $data['title'] = "Accueil | Configuration " ;
        
        $page = $this->load->view('V_Plage', $data, true); //On charge la vue pour le contenu de la page mais on bloque l'affichage dans le navigateur
			
        $this->load->view('commun/V_template', array('content_page' => $page)); //On charge la vue du template et on passse en données le contenu de la vue précédentes
	}
}
