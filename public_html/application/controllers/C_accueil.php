<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_accueil extends CI_Controller {

	public function __construct() 
    {
        
        parent::__construct();
        
        $this->load->model('M_connexion') ; //On charge le modele M_connexion
        
        $this->load->helper('url');

        session_start() ; //Chargement de l'environnement de session pour travailler avec 
            
    }
    
	public function index()
	{  
        
        $data['title'] = "Accueil | Configuration " ;

        if(isset($_SESSION['user']) && !empty($_SESSION['user']))  //Vérification de la variable de session pour savoir si l'utilisateur est connecté
        {

            redirect('C_config');   //Redirection sur la page de de l'utilisateur

        } else {

            /*******************************************************Formulaire de Connexion*******************************************************************************/

            if($_SERVER['REQUEST_METHOD'] == 'POST')  //Sécurité on vérifie que la méthode est bien en POST et pas autre chose
            {

                if(isset($_POST['connect'])) //On verifie si le formulaire à est été déclenché (lors qu'on appuie sur le bouton "Connexion")
                {

                    if(isset($_POST['login']) && isset($_POST['password'])) //On vérifie si les varaibles login et password du formulaire existe 
                    {

                        if (!empty($_POST['login']) AND ! empty($_POST['password']))  //On vérifie si elle ne sont pas vide 
                        {

                            $login = htmlspecialchars($_POST['login']) ;    //htmlspecialchars sert pour vérfier qu'on a pas d'injection de code javascript dans le formulaire

                            $password = htmlspecialchars($_POST['password']) ; 
                            
                            $request = $this->M_connexion->connect($login, sha1($password)) ;  //On execute la fonction du models 
                            
                            if($request != null){

                                $_SESSION['user'] = $request[0] ;

                                header('Location:http://92.89.196.2/beachconfig/index.php/C_config') ; 

                            }else {

                                $error = "L'email ou le mot de passe utilisé est incorrect, veuillez réessayer." ;
                                
                            }

                        } else {

                            $error = "L'email ou le mot de passe utilisé est incorrect, veuillez réessayer." ;

                        }

                    } else {

                        $error = "L'email ou le mot de passe utilisé est incorrect, veuillez réessayer." ;

                    }
    
                }

            } 

            if(isset($error))
            {
                $data['error'] = $error ;
            }

            /*************************************************On charge les vues (Affichage dans le navigateur)***************************************************************/

            $page = $this->load->view('V_plage', $data, true); //On charge la vue pour le contenu de la page mais on bloque l'affichage dans le navigateur
                
            $this->load->view('commun/V_template', array('content_page' => $page)); //On charge la vue du template et on passse en données le contenu de la vue précédentes

        }
	}
}
