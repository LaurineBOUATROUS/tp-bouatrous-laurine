<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

  function __construct() {
      
    parent::__construct();
      
    if(!$this->session->userdata('username')) redirect('admin');
      
  }


  public function index() {
      
    $this->load->spark('php-activerecord/0.0.2');
    $this->load->view('_header');

    if($this->session->userdata('isadmin')) $this->load->view('V_Plage');
    else $this->load->view('Others');

    $this->load->view('_footer');
      
  }
    
}