<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_config extends CI_Controller {

	public function __construct() 
    {
        
        parent::__construct();
        
        $this->load->model('M_connexion') ;  //On charge le modele M_connexion

        $this->load->model('M_gestion') ; 
        
        $this->load->helper('url');

        session_start() ;  //Chargement de l'environnement de session pour travailler avec 
            
    }
    
	public function index()
	{  

        if(isset($_SESSION['user']) && !empty($_SESSION['user']))  //Vérification de la variable de session pour savoir si l'utilisateur est connecté
        {

            $data['title'] = "Configuration" ; //Titre de la page en html

            $IDplage = 1 ; 

            $resultMsg = $this->M_gestion->select_msg_by_date($IDplage);

            $data['resultMsg'] = $resultMsg ;
            
            $page = $this->load->view('V_utilisateur', $data, true); //On charge la vue pour le contenu de la page mais on bloque l'affichage dans le navigateur
                
            $this->load->view('commun/V_template', array('content_page' => $page)); //On charge la vue du template et on passse en données le contenu de la vue précédentes

        } else { //Si l'utilisateur n'est pas connecté

            header('Location: http://92.89.196.2/beachconfig/') ; //Redirection sur la page de connexion

        }
        
    }
    
    public function disconnect()
    {
        
        $this->M_connexion->disconnect();

		redirect($_SERVER['HTTP_REFERER']);
    }
}