<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_gestion extends CI_Model {

    public function __construct() {

        parent::__construct();

        $this->load->database(); //On charge la config contenue dans le fichier database.php

    }

    //Fonction pour select tous les messages pour une plage (on spécifie l'id de la plage en paramètre de la fonction)

    public function select_msg_by_date($prmIDplage)
    {
        $result = $this->db->select('*')        //Requete SLQ notation spécifique à codeIgniter
                        ->from('messageperso')
                        ->where('IDplage', $prmIDplage)   
                        ->get()
                        ->result_array() ; 

        return $result ; //On retourne le résultat de la fonction

    }
    
}