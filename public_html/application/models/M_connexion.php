<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_connexion extends CI_Model {

    public function __construct() {

        parent::__construct();

        $this->load->database(); //On charge la config contenue dans le fichier database.php

    }

    //Fonction exécuter lors de la connexion de l'utilisateur afin de vérifier s'il existe

    public function connect($prmLogin, $prmPassword)
    {
        $result = $this->db->select('*')
                        ->from('users')
                        ->where('login', $prmLogin)
                        ->where('pass', $prmPassword)
                        ->get()
                        ->result_array() ; 

        if(count($result) !== 1)
        {
            $result = null ;
        }

        return $result ;

    }

    //Fonction exécuter lors de la déconnexion de l'utilisateur

    public function disconnect()
    {
        $_SESSION = array();

		session_destroy();
    }
    
}