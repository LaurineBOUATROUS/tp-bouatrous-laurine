<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'C_accueil';   //C'est le controllers par défaut qui se sera charger par l'index.php de Codeigniter..
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
