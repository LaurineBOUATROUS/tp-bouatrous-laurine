<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$active_group = 'default';
$query_builder = TRUE;

$db['default'] = array(
	'dsn'	=> 'mysql:host=localhost;dbname=bddplage_projet;charset=utf8',  //type de connexion et on spécifie à quelle base de données on se connecte
	'hostname' => 'localhost',   //connexion en localhost donc non à distance
	'username' => 'user_plage',  //nom d'utilisateur
	'password' => 'plage_projet',  //mot de passe de l'utilisateur
	'database' => '',
	'dbdriver' => 'pdo',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);
